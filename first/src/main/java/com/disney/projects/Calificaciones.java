package com.disney.projects;

import java.util.Scanner;

public class Calificaciones {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		double suma, promedio;
		System.out.println("Nombre del alumno ");
		String nom = in.next();

		System.out.println("primer evaluación");
		double num1 = in.nextDouble();

		System.out.println("Segunda evaluación");
		double num2 = in.nextDouble();

		System.out.println("tercera evaluación");
		double num3 = in.nextDouble();

		suma = num1 + num2 + num3;
		promedio= suma/3;

		System.out.println(nom + " su promedio es de: " + promedio);

		if (promedio >= 1 &&  promedio <= 6) {
			System.out.println("Usted reprobo ");
		} else if (promedio >= 7 && promedio <= 10) {
			System.out.println("usted aprobo");
		} else
			System.out.println("no existe la calificación");

	}

}
