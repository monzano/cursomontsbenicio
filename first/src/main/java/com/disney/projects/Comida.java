package com.disney.projects;
import java.util.Scanner;

public class Comida {
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		
		System.out.println("Museos de la Cuidad de México: \n 1.Centro Cultural Universitario Tlatelolco \n 2.Museo Antiguo Colegio de San Ildefonso \n 3.Museo de Arte Moderno");
		int num=in.nextInt();
		
		switch (num) {
		case 1:
			System.out.println("el Centro Cultural Universitario Tlatelolco ofrece tres exposiciones permanentes: \n la muestra multimedia Memorial del 68, Colección Stavenhagen –de arte prehispánico– y Museo Tlatelolco, conformada por piezas prehispánicas localizadas en la zona arqueológica adyacente al foro.");
			break;
		case 2:
			System.out.println("se encuentran diversos murales de artistas como Jean Charlot, David Alfaro Siqueiros, José Clemente Orozco y “La Creación”, el primer mural de Diego Rivera. Además, en este espacio se ha expuesto arte virreinal, moderno y contemporáneo. \n Entre sus exposiciones recientes resaltan la de las esculturas hiperrealistas gigantes de Ron Mueck, el pensamiento de José Saramago, la obra fascinante de Antony Garmley o las pinturas de Marilyn Manson, entre muchos otros.");
			break;
		case 3:
			System.out.println("Sigue siendo, a la fecha, uno de los recintos más importantes de Latinoamérica en el arte del siglo XX, pues además de ofrecer exposiciones temporales de muy alta calidad, cuenta con una gran colección propia de artistas como Diego Rivera, Leonora Carrington, Frida Kahlo, y Remedios Varo.\n Tiene también una importante colección de placas del fotógrafo mexicano Manuel Álvarez Bravo.");
			break;
		default:
			System.out.println("Verifique su número");
			break;
		}
	}
}
