package com.disney.projects;

import java.util.Scanner;

public class Hipotenusa {

	public static void process(double a, double b) {
		double result1 = Math.pow(a, 2) + Math.pow(b, 2);
		double result = Math.sqrt(result1);
		System.out.println("Cateto c = " + result);
	}

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);

		System.out.println("Value of cateto a");
		double a = in.nextDouble();

		System.out.println("Value of cateto b");
		double b = in.nextDouble();

		process(a, b);
	}

}
