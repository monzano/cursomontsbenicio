package com.disney.projects.abstracts;

public class Eyeliner extends Store{
private int total;
	
	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
	public Eyeliner(String product, int cost, int quantity){
		super(product,cost,quantity);
	}
	@Override
	public void calculateTotal() {
		setTotal(getCost() * getQuantity());
		System.out.println("Total= "+getTotal());
		
	}

}
