package com.disney.projects.abstracts;

import java.util.ArrayList;
import java.util.List;

public class InvokerStore {

	public static void main(String[] args) {
		
		Lip lip = new Lip("Mate",30,50);
		lip.statusStore();
		
		System.out.println("----------------");
		
		Eyeliner eyeliner= new Eyeliner("pincel",40,8);
		eyeliner.statusStore();
		
		System.out.println("----------------");
		
		List<Store> storeList= new ArrayList<Store>();
		
		storeList.add(lip);
		storeList.add(eyeliner);
		
		for (Store store : storeList) {
			store.statusStore();
			System.out.println("=============");
		}
	}

}
