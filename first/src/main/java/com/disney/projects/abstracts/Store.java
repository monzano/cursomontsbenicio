package com.disney.projects.abstracts;

public abstract class Store {

	private String product;
	private int cost;
	private int total;
	private int quantity;

	public Store() {

	}

	public Store(String product, int cost, int quantity) {
		this.product=product;
		this.cost=cost;
		this.quantity=quantity;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void statusStore() {
		System.out.println(getProduct());
		System.out.println(getCost());
		System.out.println(getQuantity());
		setTotal(getCost() * getQuantity());

		calculateTotal();
	}

	public abstract void calculateTotal();

}
