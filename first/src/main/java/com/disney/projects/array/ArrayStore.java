package com.disney.projects.array;

public class ArrayStore {
	private int table;
	private int add;
	
	public ArrayStore(){
		
	}
	public ArrayStore(int table, int add){
		this.add=add;
		this.table=table;
	}
	public int getAdd(){
		return add;
	}
	public void setAdd(int add){
		this.add=add;
	}
	public int getTable(){
		return table;
	}
	public void setTable(int table){
		this.table=table;
	}
	
	public void showProduct(String[] arrayStore){
		for (String name : arrayStore) {
			System.out.println(name);
		}
			
	}
	
	public void table(int[] arrayTable){
		for(int i=0;i<arrayTable.length;i++){
			arrayTable[i]=getTable()*add++;
			System.out.println(i+"X"+getTable()+"="+ arrayTable[i]);
		}
	}

}
