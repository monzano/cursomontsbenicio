package com.disney.projects.figures;

import com.disney.projects.interfaces.Agency;

public class Cars implements Agency{
	String agency;
	String name;
	int cost;

	public Cars() {

	}

	public Cars(String agency, String name, int cost) {
		this.agency=agency;
		this.name=name;
		this.cost=cost;
	}
	
	public String getAgency(){
		return agency;
	}
	public void setAgency(String agency){
		this.agency=agency;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name=name;
	}
	
	public int getCost(){
		return cost;
	}
	public void setCost(int cost){
		this.cost=cost;
	}
	
	
	public void returnAgency() {
		System.out.println("car information");
		
	}

	@Override
	public String toString() {
		return "Cars [agency=" + agency + ", name=" + name + ", cost=" + cost + "]";
	}

	@Override
	public boolean equals(Object obj) {
		 
		if(obj != null && obj instanceof Cars ){
			Cars other=((Cars)obj);
			return other.agency == this.agency && other.name == this.name && other.cost == this.cost;
		}
		return false;
	}
	
	
	


}
