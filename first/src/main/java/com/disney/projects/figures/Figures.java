package com.disney.projects.figures;

public class Figures {
	
	private int area;
	private int perimeter;
	
	public Figures(){
		
	}
	public Figures(int area, int perimeter){
		this.area=area;
		this.perimeter=perimeter;
	}
	
	public int getArea(){
		return area;
	}
	public void setArea(int area){
		this.area=area;
	}
	
	public int getPerimeter(){
		return perimeter;
	}
	public void setPerimeter(int perimeter){
		this.perimeter=perimeter;
	}

}
