package com.disney.projects.figures;

public class FirstCar extends Cars {
	
	public FirstCar(String agency, String name, int cost) {
		super(agency,name,cost);
	}
	
	@Override
	public void returnAgency() {
		System.out.println("Su carro es: "+ getAgency()  );	
	}

	@Override
	public String toString() {
		return "FirstCar [agency=" + getAgency() + ", name=" + getName() + ", cost=" + getCost() + "]";
	}
	
	

}
