package com.disney.projects.figures;

public class FirstSoda extends Soda {
	
	public FirstSoda(String name,int cost) {
		super(name,cost);
	}

	@Override
	public void companySoda() {
		System.out.println("It's too good");
	}
	
	@Override
	public String toString() {
		return "FisrtSoda [name=" + getName() + ", cost=" + getCost() +  "]";
	}


	

}
