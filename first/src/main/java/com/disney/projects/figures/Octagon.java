package com.disney.projects.figures;

import com.disney.projects.interfaces.Figurables;

public class Octagon extends Figures implements Figurables {
	private int apothem;
	private int side;
	private static final int OCTAGON_SIDES = 8;

	public Octagon() {

	}

	public Octagon(int side, int apothem) {
		this.apothem=apothem;
		this.side=side;
	}

	public int getApothem() {
		return apothem;
	}

	public void setApothem(int apothem) {
		this.apothem = apothem;
	}

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}
	
	public void calculateArea(){
		this.setArea(OCTAGON_SIDES*getSide()*getApothem());
	}
	
	public void calculatePerimeter(){
		this.setPerimeter(OCTAGON_SIDES*getSide());
	}


}
