package com.disney.projects.figures;

public class SecondCar extends Cars {
	
	public SecondCar(String agency, String name, int cost) {
		super(agency,name,cost);
	}
	
	@Override
	public void returnAgency() {
		System.out.println("car information SecondCar");	
	}

	@Override
	public String toString() {
		return "SecondCar [agency=" + getAgency() + ", name=" + getName() + ", cost=" + getCost() + "]";
	}
	
	

}
