package com.disney.projects.figures;

public class SecondSoda extends Soda {
	
	public SecondSoda(String name,int cost) {
		super(name,cost);
	}

	@Override
	public void companySoda() {
		System.out.println("It's too good");
	}
	
	@Override
	public String toString() {
		return "SecondSoda [name=" + getName() + ", cost=" + getCost() + "]";
	}


}
