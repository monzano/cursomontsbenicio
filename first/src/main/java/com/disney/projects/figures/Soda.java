package com.disney.projects.figures;

import com.disney.projects.interfaces.Company;

public class Soda implements Company {

	private int cost;
	private String name;

	public Soda() {

	}

	public Soda(String name,int cost) {
		this.cost = cost;
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getName() {
		return name;
	}

	public void setCost(String name) {
		this.name = name;
	}

	public void companySoda() {
		System.out.println("It's too good");

	}

	@Override
	public String toString() {
		return "Soda [name=" + name + ", cost=" + cost + "]";
	}

	@Override
	public boolean equals(Object obj) {

		if (obj != null && obj instanceof Soda) {
			Soda other = ((Soda) obj);
			return other.cost == this.cost && other.name.equals(this.name);
		}
		return false;
	}
	
}
