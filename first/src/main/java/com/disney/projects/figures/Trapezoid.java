package com.disney.projects.figures;
import com.disney.projects.interfaces.Figurables;

public class Trapezoid extends Figures implements Figurables{
	
	private int tall;
	private int base;
	private int baseHigher;
	private int side;

	public Trapezoid() {

	}

	public Trapezoid(int tall, int base, int baseHigher, int side) {
		this.tall=tall;
		this.base=base;
		this.baseHigher=baseHigher;
		this.side=side;
	}
	
	public int getTall(){
		return tall;
	}
	public void setTall(int tall){
		this.tall=tall;
	}
	public int getBase(){
		return base;
	}
	public void setBase( int base){
		this.base=base;
	}
	public int getBaseHigher(){
		return baseHigher;
	}
	public void setBaseHigher(int baseHigher){
		this.baseHigher=baseHigher;
	}
	public int getSide(){
		return side;
	}
	public void setSide(int side){
		this.side=side;
	}
	
	public void calculateArea(){
		this.setArea(getTall()*(getBase()+getBaseHigher())/2);
	}
	public void calculatePerimeter(){
		this.setPerimeter(getSide()+getSide()+getBase()+getBaseHigher());
	}

}
