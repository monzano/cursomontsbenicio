package com.disney.projects.hierachy;

import java.util.ArrayList;
import java.util.List;

import com.disney.projects.figures.Cars;
import com.disney.projects.figures.FirstCar;
import com.disney.projects.figures.SecondCar;

public class HierachyCar {

	public static void main(String[] args) {
		
		Cars cars = new Cars("BMW", "M4 Coupé ", 1574900);
		Cars carstwo = new Cars("BMW", "M4 Coupé ", 1574900);

		cars.returnAgency();
		System.out.println(cars.getAgency());
		System.out.println(cars.getName());
		System.out.println(cars.getCost());
		System.out.println("-----------------------");

		FirstCar firstCar = new FirstCar("Toyota", "Camry ", 418000);

		firstCar.returnAgency();
		System.out.println(firstCar.getAgency());
		System.out.println(firstCar.getName());
		System.out.println(firstCar.getCost());
		System.out.println("-----------------------");

		SecondCar secondCar = new SecondCar("Honda", " Accord Sedan ", 448900);

		secondCar.returnAgency();
		System.out.println(secondCar.getAgency());
		System.out.println(secondCar.getName());
		System.out.println(secondCar.getCost());
		System.out.println("-----------------------");

		System.out.println(cars);
		System.out.println(firstCar);
		System.out.println(secondCar);
		System.out.println("-----------------------");
		
		List<Cars> car = new ArrayList<Cars>();
		car.add(cars);
		car.add(firstCar);
		car.add(secondCar);
		
		System.out.println(car);
		System.out.println("-----------------------");
		
		if(cars.equals(carstwo)){
			System.out.println("Son iguales");
		}else{
			System.out.println("no son ");
		}
			
		car.remove(carstwo);
		System.out.println(car);
		
		
		
	}

}
