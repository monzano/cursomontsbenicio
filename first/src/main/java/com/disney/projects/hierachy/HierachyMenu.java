package com.disney.projects.hierachy;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import com.disney.projects.figures.Soda;
import com.disney.projects.figures.FirstSoda;
import com.disney.projects.figures.SecondSoda;
public class HierachyMenu {
	private static List<Soda> sodaList = new ArrayList<Soda>();
	public static void main(String[] args) {

		Scanner sca = new Scanner(System.in);
		String rep = "no";

		Soda soda = new Soda("Pepsi", 12);
		FirstSoda firstSoda = new FirstSoda("Senzao", 10);
		SecondSoda secondSoda = new SecondSoda("Coca-cola", 15);
		
		
		sodaList.add(soda);
		sodaList.add(firstSoda);
		sodaList.add(secondSoda);

		do {
			System.out.println(sodaList);

			System.out.println("1.- Add soda");
			System.out.println("2.- Delete soda");
			int opt = sca.nextInt();
			switch (opt) {
			case 1:
				System.out.println("Add Soda 1.-Soda(Father)     2.-FirstSoda    3.-SecondSoda");
				int option = sca.nextInt();
				
				System.out.println("Name of the soda?");
				String name = sca.next();
				System.out.println("Cost of the soda?");
				int cost = sca.nextInt();
				
				if(option ==1){
					sodaList.add(new Soda (name,cost));					
				}else if(option==2){
					sodaList.add(new FirstSoda (name,cost));
				}else if(option==3){
					sodaList.add(new SecondSoda (name,cost));
				}
				
				System.out.println(sodaList);
				
				break;//case 1
			case 2:
				System.out.println("Delete Soda 1.-Soda(Father)     2.-FirstSoda    3.-SecondSoda");
				int option2 = sca.nextInt();
				
				System.out.println("Name of the soda?");
				name = sca.next();
				System.out.println("Cost of the soda?");
				cost = sca.nextInt();
				
				if(option2==1){
					deleteSoda(new Soda(name,cost));
				}else if(option2==2){
					deleteSoda(new FirstSoda(name,cost));
				}else if(option2==3){
					deleteSoda(new SecondSoda(name,cost));
				}
				
				System.out.println(sodaList);
				
			default:
				break;
			}
			System.out.println("you want to repeat the menu? yes/no");
			rep = sca.next();
		} while (rep.equalsIgnoreCase("yes"));
	}

	public static void deleteSoda(Soda soda) {
		sodaList.remove(soda);
	}

}
