package com.disney.projects.hierachy;

import com.disney.projects.figures.*;
import java.util.Scanner;

public class ReturnHierachy {

	public static void main(String[] args) {
		
		Scanner sca=new Scanner(System.in);
		Trapezoid trapezoid = new Trapezoid();
		Octagon octagon = new Octagon();
		String rep="no";
		
		
		do{
		System.out.println("1.-Trapezoid area");
		System.out.println("2.-Trapezoid perimeter");
		System.out.println("3.-Octagon area");
		System.out.println("4.-Octagon Perimeter");
		System.out.println("5.-Octagon area and perimeter");
		int opt=sca.nextInt();
		switch (opt) {
		case 1:
			System.out.println("size of tall?");
			int tall=sca.nextInt();
			trapezoid.setTall(tall);
			
			System.out.println("size of base?");
			int base=sca.nextInt();
			trapezoid.setBase(base);
			
			System.out.println("size of base Higher?");
			int baseHigher=sca.nextInt();
			trapezoid.setBaseHigher(baseHigher);
			
			trapezoid.calculateArea();
			
			System.out.println("Trapezoid area= "+trapezoid.getArea()); 
			
			break;
		case 2:
			System.out.println("size of side?");
			int side=sca.nextInt();
			trapezoid.setSide(side);
			
			System.out.println("size of base?");
			base=sca.nextInt();
			trapezoid.setBase(base);
			
			System.out.println("size of base Higher?");
			baseHigher=sca.nextInt();
			trapezoid.setBaseHigher(baseHigher);
			
			trapezoid.calculatePerimeter();
			
			System.out.println("Trapezoid perimeter" + trapezoid.getPerimeter());
			
			break;
		case 3:
			System.out.println("size of apothem?");
			int apothem=sca.nextInt();
			octagon.setApothem(apothem);
			
			System.out.println("size of side?");
			side=sca.nextInt();
			octagon.setSide(side);
			
			octagon.calculateArea();
			
			System.out.println("Octagon area= " + octagon.getArea());
			 break;
		case 4:
			System.out.println("size of side?");
			side=sca.nextInt();
			octagon.setSide(side);
			
			octagon.calculatePerimeter();
			
			System.out.println("Octagon Perimeter= "+ octagon.getPerimeter());
			break;
		case 5:
			Octagon octa = new Octagon(6,2);
			
			octa.calculateArea();
			octa.calculatePerimeter();
			
			System.out.println("Octagon area= " + octa.getArea());
			System.out.println("Octagon Perimeter= "+ octa.getPerimeter());
			break;

		default:
			
			break;
		}
		System.out.println("you want to repeat the menu? yes / no");
		rep=sca.next();
		
		}while(rep.equalsIgnoreCase("yes"));
		System.out.println("oktl");
	}

}
