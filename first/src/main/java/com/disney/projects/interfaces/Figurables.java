package com.disney.projects.interfaces;

public interface Figurables {
	
	void calculateArea();
	void calculatePerimeter();

}
