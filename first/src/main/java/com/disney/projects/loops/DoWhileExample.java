package com.disney.projects.loops;

public class DoWhileExample {
		
	private String name;
	private int number;
	private int number2;
	private int number3;
	private int add;
	private int result;
	private String person;
	
	public DoWhileExample(){}
	public DoWhileExample(String name, int number, int number2, int number3, int add, int result, String person) {
		this.name = name;
		this.number = number;
		this.number2 = number2;
		this.number3 = number3;
		this.add = add;
		this.result = result;
		this.person=person;
	}
	
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name=name;
	}
	public int getNumber(){
		return number;
	}
	public void setNumber(int number){
		this.number=number;
	}
	public int getNumber2(){
		return number2;
	}
	public void setNumber2(int number2){
		this.number2=number2;
	}
	public int getNumber3(){
		return number3;
	}
	public void setNumber3(int number3){
		this.number3=number3;
	}
	public int getAdd(){
		return add;
	}
	public void setAdd(int add){
		this.add=add;
	}
	public int getResult(){
		return result;
	}
	public void setResult(int result){
		this.result=result;
	}
	public String getPerson(){
		return person;
	}
	public void setPerson(String person){
		this.person=person;
	}
	
	public void execute(){
		
			setAdd(getNumber()+getNumber2()+getNumber3());
			setResult(getAdd()/3);
			
			 System.out.println(getName() + "\n Your qualification is: " + getResult());
			if (getResult() >= 1 &&  getResult() <= 6) {
				System.out.println("reprove the matter ");
			} else if (getResult() >= 7 && getResult() <= 10) {
				System.out.println("approve the matter");
			} else
				System.out.println("the appraisal does not exist");
			
		
	}
	
}
