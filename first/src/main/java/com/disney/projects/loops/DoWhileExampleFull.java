package com.disney.projects.loops;

import com.disney.projects.loops.DoWhileExample;

import java.lang.invoke.SwitchPoint;
import java.util.Scanner;

public class DoWhileExampleFull {

	public static void main(String[] args) {

		String person = "No";

		DoWhileExample doWhileExample = new DoWhileExample();
		ForExample forExample = new ForExample();
		ForTable forTable = new ForTable();

		Scanner sca = new Scanner(System.in);
		String rep = "yes";

		while (rep.equalsIgnoreCase("yes")) {

			System.out.println(
					"Enter the option you want to make: \n 1.enter qualifications \n 2.working register \n 3.Multiplication table ");
			int opc = sca.nextInt();
			switch (opc) {
			case 1:
				do {
					System.out.println("Name of student");
					String name = sca.next();
					doWhileExample.setName(name);

					System.out.println("first evaluation");
					int number = sca.nextInt();
					doWhileExample.setNumber(number);

					System.out.println("second evaluation");
					int number2 = sca.nextInt();
					doWhileExample.setNumber2(number2);

					System.out.println("third evaluation");
					int number3 = sca.nextInt();
					doWhileExample.setNumber3(number3);

					doWhileExample.execute();
					System.out.println("repeat the procedure ? yes o no");
					person = sca.next();
					doWhileExample.setPerson(person);
				} while (doWhileExample.getPerson().equalsIgnoreCase("yes"));
				break;
			case 2:
				for (int i = 1; i <= 4; i++) {
					System.out.println(
							"The workers of the company disney must comply with 35 hours a week so that their weekly salary is equivalent to $7000.\n The company allows them to work maximum 5 overtime hours which are paid at double.");
					System.out.println("¿How many overtime hours work? \n 1 Hr. \n 2 Hr. \n 3 Hr. \n 4 Hr. \n 5 Hr.");
					int num2 = sca.nextInt();
					forExample.setNum2(num2);
					forExample.work();
				}
				break;

			case 3:
				System.out.println("which table do you want to know?");
				int num = sca.nextInt();
				forTable.setNum(num);

				forTable.executeTable();
				break;
			default:
				System.out.println("check the options ");
			
			}

			System.out.println("you want to repeat the menu? yes o no");
			rep=sca.next();
		}
	}

}
