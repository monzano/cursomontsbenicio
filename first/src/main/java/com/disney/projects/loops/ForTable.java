package com.disney.projects.loops;

public class ForTable {
	private int num;
	
	public ForTable(){
		
	}
	public ForTable(int num){
		this.num=num;
	}
	public int getNum(){
		return num;
	}
	public void setNum(int num){
		this.num=num;
	}
	
	public void executeTable(){
		for (int i=1; i<=10; i++){
			System.out.println(getNum()+" X " + i + " = " +(getNum()*i));
		}
	}
	

}
