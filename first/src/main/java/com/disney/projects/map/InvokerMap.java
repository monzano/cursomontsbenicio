package com.disney.projects.map;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.disney.projects.figures.FirstSoda;
import com.disney.projects.figures.SecondSoda;
import com.disney.projects.figures.Soda;

public class InvokerMap {

	public static void main(String[] args) {

		String rep="no";
		Scanner sca = new Scanner(System.in);
		Soda soda = new Soda("Pepsi", 12);
		FirstSoda firstSoda = new FirstSoda("Senzao", 10);
		SecondSoda secondSoda = new SecondSoda("Coca-cola", 15);

		Map<Integer, Soda> sodas = new HashMap<Integer, Soda>();
		sodas.put(1, soda);
		sodas.put(2, firstSoda);
		sodas.put(3, secondSoda);

		do{
			System.out.println("key:");
			System.out.println(sodas.keySet());
			System.out.println("content");
			System.out.println(sodas.values());
			System.out.println("enter number  1.-Add Soda  2.-Delete Soda  3.-Search Soda");
			int option = sca.nextInt();
			switch (option) {
			case 1:
				System.out.println("Add Soda 1.-Soda(Father)     2.-FirstSoda    3.-SecondSoda");
				int opt = sca.nextInt();
				
				System.out.println("refresh key");
				int clave = sca.nextInt();
				System.out.println("Name of the soda?");
				String name = sca.next();
				System.out.println("Cost");
				int cost = sca.nextInt();

				if (opt == 1) {
					sodas.put(clave, new Soda(name, cost));
				} else if (opt == 2) {
					sodas.put(clave, new FirstSoda(name, cost));
				} else if (opt == 3) {
					sodas.put(clave, new SecondSoda(name, cost));
				}

				System.out.println(sodas.get(clave));

				break;
			case 2:

				System.out.println("Delete Soda 1.-Soda(Father)     2.-FirstSoda    3.-SecondSoda");
				int opt1 = sca.nextInt();
				System.out.println("refresh key");
				clave = sca.nextInt();
				System.out.println("Name of the soda?");
				name = sca.next();
				System.out.println("Cost ");
				cost = sca.nextInt();

				if (opt1 == 1) {
					sodas.remove(clave, new Soda(name, cost));
				} else if (opt1 == 2) {
					sodas.remove(clave, new FirstSoda(name, cost));
				} else if (opt1 == 3) {
					sodas.remove(clave, new SecondSoda(name, cost));
				}

				System.out.println(sodas.keySet());

				break;

			case 3:
				
				System.out.println("refresh key");
				clave = sca.nextInt();
				
				System.out.println(sodas.get(clave));
				

				break;

			default:
				System.out.println("does not exist");
				break;
			}

			System.out.println("you want to repeat the menu? yes/no");
			rep = sca.next();
		}while(rep.equalsIgnoreCase("yes"));
		}

}
